import asyncio
import os
import sys
import threading
import tkinter

BLACK = (0, 0, 0)
NUM_LEDS = int(os.environ.get('NUM_LEDS', 150))
RUNNING = 1

def tuple_to_colour_string(colour_tuple):
    red = colour_tuple[0] & 0xFF
    green = colour_tuple[1] & 0xFF
    blue = colour_tuple[2] & 0xFF
    padding = 2
    return "#{:02x}{:02x}{:02x}".format(red, green, blue)


class ProfitMonitor(tkinter.Frame):
    INITIAL_WIDTH = 130
    INITIAL_HEIGHT = 300

    def __init__(self, num_leds):
        self.root = tkinter.Tk()
        self.root.geometry("{}x{}".format(self.INITIAL_WIDTH, self.INITIAL_HEIGHT))
        super().__init__(self.root, width=self.INITIAL_WIDTH, height=self.INITIAL_HEIGHT)
        self.pack()
        self.num_leds = num_leds
        self.led_objects = {}
        self.canvas = tkinter.Canvas(self, width=self.INITIAL_WIDTH, height=self.INITIAL_HEIGHT)
        self.master.title("Profit Monitor Emulator")
        self.pack(fill=tkinter.BOTH, expand=1)
        self.bind("<Configure>", self.configure)
        self.led_array = [BLACK] * self.num_leds

    def set_leds(self, led_array):
        self.led_array = led_array
        self.configure()

    def configure(self, foo=None):
        if foo:
            self.width = foo.width
            self.height = foo.height
        led_height = self.height / float(self.num_leds)
        for i in range(self.num_leds):
            if i not in self.led_objects:
                rect = self.canvas.create_rectangle(
                    0, led_height * i, self.width - 1, led_height * i + led_height,
                    outline=tuple_to_colour_string(BLACK),
                    fill=tuple_to_colour_string(BLACK)
                )
                self.led_objects[i] = rect
            else:
                self.canvas.itemconfigure(
                    self.led_objects[i],
                    outline=tuple_to_colour_string(BLACK),
                    fill=tuple_to_colour_string(self.led_array[i])
                )
                self.canvas.coords(
                    self.led_objects[i],
                    0, led_height * i,
                    self.width - 1,
                    led_height * i + led_height
                )
        self.canvas.pack(fill=tkinter.BOTH, expand=tkinter.YES)


class ProfitStateMachine(object):
    WAITING_FOR_COMMAND = "waiting_for_command"
    WAITING_FOR_LENGTH = "waiting_for_length"
    READING_DATA = "reading_data"

    def __init__(self, num_leds, profit_monitor):
        self.num_leds = num_leds
        self.profit_monitor = profit_monitor

        self.state = self.WAITING_FOR_COMMAND
        self.led_colours = [BLACK] * self.num_leds
        self.length_buffer = []
        self.length = 0
        self.data_buffer = []

    async def reset(self):
        self.led_colours = [BLACK] * self.num_leds
        self.profit_monitor.set_leds(self.led_colours)
        self.state = self.WAITING_FOR_COMMAND
        await asyncio.sleep(0)

    async def handle_byte(self, data_byte):
        if self.state == self.WAITING_FOR_COMMAND:
            # command
            if data_byte == 0x00:
                # we're expecting colour data
                self.state = self.WAITING_FOR_LENGTH
            elif data_byte == 0xFF:
                # update the display
                self.state = self.WAITING_FOR_COMMAND
            else:
                raise ValueError("Invalid command")

        elif self.state == self.WAITING_FOR_LENGTH:
            self.length_buffer.append(data_byte)
            if len(self.length_buffer) == 2:
                self.state = self.READING_DATA
                self.length = 3 * ((self.length_buffer[0] << 8) | self.length_buffer[1])
                self.length_buffer.clear()
                if self.length == 0:
                    self.state = self.WAITING_FOR_COMMAND
                elif self.length != 3 * self.num_leds:
                    await self.reset()
                    raise ValueError("Wrong length")
                else:
                    self.state = self.READING_DATA
        elif self.state == self.READING_DATA:
            self.data_buffer.append(data_byte)
            self.length -= 1
            if self.length == 0:
                # we have all the data
                led_colours_temp = []
                while self.data_buffer:
                    if len(self.data_buffer) >= 3:
                        colour = (self.data_buffer.pop(0), self.data_buffer.pop(0), self.data_buffer.pop(0))
                        led_colours_temp.append(colour)
                    else:
                        await self.reset()
                led_colours_temp.reverse()
                self.led_colours = led_colours_temp
                self.profit_monitor.set_leds(self.led_colours)
                self.state = self.WAITING_FOR_COMMAND
                await asyncio.sleep(0)


class ProfitMonitorServer(object):
    def __init__(self, num_leds, profit_monitor):
        self.state_machine = ProfitStateMachine(num_leds, profit_monitor)

    async def handle_client(self, reader, writer):
        while True:
            data = await reader.read(2048)
            print("Received {!r}".format(data))
            if data:
                for datum in data:
                    try:
                        await self.state_machine.handle_byte(datum)
                    except ValueError as e:
                        print(e)
                        writer.close()
                        return
            else:
                print("Received no data!")
                await self.state_machine.reset()
                writer.close()
                return

    def create_task(self):
        return asyncio.start_server(self.handle_client, "0.0.0.0", 2812)

def create_server(gui):
    server = ProfitMonitorServer(NUM_LEDS, gui)
    loop = asyncio.new_event_loop()
    loop.create_task(server.create_task())
    loop.run_forever()

def main():
    gui = ProfitMonitor(NUM_LEDS)

    server_thread = threading.Thread(target=create_server, args=(gui,))
    server_thread.setDaemon(True)
    server_thread.start()

    gui.root.mainloop()


if __name__ == '__main__':
    main()
