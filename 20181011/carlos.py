import time
import random


def make_list(x, num_leds):
	led_list = [(255,255,255)] * num_leds
	if x < 50:
		led_list[x:x+10] = [(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255)]
	else:
		led_list[x:x+10] = [(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0)]
	
	return led_list


WHITE = (255,255,255)
BLUE = (0,0,255)
RED = (255,0,0)
BLACK = (0,0,0)
GREEN = (0,255,0) 
YELLOW = (0,255,255)
RANDOM = (int(random.random()*255), int(random.random()*255), int(random.random()*255))

def setled(leds,index,colour):

	leds[index] = colour

	return leds


def run(client, num_leds):

	# led_list = [(0,0,0)]*num_leds
	
	for i in range(num_leds):
		led_list = [(0,0,0)]*num_leds
		RANDOM = (int(random.random()*255), int(random.random()*255), int(random.random()*255))
		setled(led_list,int(random.random()*num_leds),RANDOM)
		#time.sleep(0.5)

	# print(len(led_list))

		client.send_all(led_list)

	#    for each in range(led_list):




	
