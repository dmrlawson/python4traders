class Matrix:
	def __init__(self, matrix_as_lists):
		if not isinstance(matrix_as_lists, list):
			raise ValueError("matrix_as_lists is not a list!")

		if len(matrix_as_lists) == 0:
			raise ValueError("matrix_as_lists is empty!")

		matrix_width = len(matrix_as_lists[0])
		for row_list in matrix_as_lists:
			if not isinstance(row_list, list):
				raise ValueError(f"{row_list} is not a list!")
			if len(row_list) != matrix_width:
				raise ValueError(f"{row_list} is not the right length")

		self._matrix_data = matrix_as_lists


	def get_raw_data(self):

		return self._matrix_data

	def _add(self,j):
		new = [[0,0,0],[0,0,0],[0,0,0]]
		for a in range(len(self.get_raw_data())):
			for b in range(len(self.get_raw_data())):
				new[a][b]= self.get_raw_data()[a][b] + j.get_raw_data()[a][b]

		return Matrix(new)

	def add(self, j):
		new_matrix = self._add(j)
		self._matrix_data = new_matrix.get_raw_data()
		return self

	def add_new(self):
		return _add(self)


	def transposer(self):

		# len(a[0]) - column lengths
		# len(a) - row lengths
		a = list(self.get_raw_data())
		b = []
		for e in range(len(a)):
			b.append([None] * len(a[0]))
		# b = [[None]*(len(a))]*(len(a[0]))
		#
		#  for c in range(len(a[0])):
		#  	for r in range(len(a)):
		# 		if r > range(length(b)):
		# 			while r > range(length(b)):
		#
		#
		#  		if r > c:
		#  			a[r][c] = a[c][r]
		#  		elif r < c

		return b

		# see how many columns/rows
		# id diagonal elements, leave them in place
		# rotate other elements around the diagonal


