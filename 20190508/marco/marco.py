from data_classes import Matrix

a = Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

b = Matrix([[1,1,1], [1,1,1], [1,1,1]])

c = a.add_new(b)

print(a.get_raw_data())
print(b.get_raw_data())
print(c)


