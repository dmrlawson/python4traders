class Matrix:
	def __init__(self, matrix_as_lists):
		if not isinstance(matrix_as_lists, list):
			raise ValueError("matrix_as_lists is not a list!")

		if len(matrix_as_lists) == 0:
			raise ValueError("matrix_as_lists is empty!")

		matrix_width = len(matrix_as_lists)
		for row_list in matrix_as_lists:
			if not isinstance(row_list, list):
				raise ValueError(f"{row_list} is not a list!")
			if len(row_list) != matrix_width:
				raise ValueError(f"{row_list} is not the right length")

		self._matrix_data = matrix_as_lists

	def get_raw_data(self):
		return self._matrix_data

	def add(self, b):
		# self.get_raw_data()
		b_data = b.get_raw_data()
		for i in range(len(self._matrix_data)):
			for j in range(len(self._matrix_data[i])):
				self._matrix_data[i][j] += b_data[i][j]

	def add_new(self, b):
		b_data = b.get_raw_data()
		new_matrix = []
		for i in range(len(self._matrix_data)):
			empty_list = []
			new_matrix.append(empty_list)
			for j in range(len(self._matrix_data[i])):
				c_element = self._matrix_data[i][j] + b_data[i][j]
				new_matrix[i].append(c_element)
		return new_matrix
