class Matrix:
	foo = 3
	def __init__(self, matrix_as_lists):
		if not isinstance(matrix_as_lists, list):
			raise ValueError("matrix_as_lists is not a list!")

		if len(matrix_as_lists) == 0:
			raise ValueError("matrix_as_lists is empty!")

		matrix_width = len(matrix_as_lists)
		for row_list in matrix_as_lists:
			if not isinstance(row_list, list):
				raise ValueError(f"{row_list} is not a list!")
			if len(row_list) != matrix_width:
				raise ValueError(f"{row_list} is not the right length")

		self._matrix_data = matrix_as_lists

	def __repr__(self):
		return str(self._matrix_data)

	def get_raw_data(self):
		return self._matrix_data

	def add(self, matrix_object):
		new_matrix = ([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
		other_list = matrix_object.get_raw_data()
		for x in range(len(other_list)):
			for y in range(len(other_list[x])):
				new_matrix = self._matrix_data[x][y] + other_list[x][y]
		return new_matrix
