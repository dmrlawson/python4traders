from data_classes import Matrix, Matrix_3_x_3

a = Matrix_3_x_3([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

b = Matrix_3_x_3([[1, 0, 0], [0, 1, 0], [0, 0, 1]])


print(a.add(b).get_raw_data())

print(a.get_raw_data())

print(a.transpose())

print(a.multiply(b))

print(b+a)

x = Matrix([[1, 2], [4, 5]])
x.print_info()

print(x.get_raw_data())

print((x+x).get_raw_data())

#z = [1,2,3,4,5]
#print(z.pop(1))
#print(z)

#print(b.determinant())