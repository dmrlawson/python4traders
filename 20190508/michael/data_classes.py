class Matrix:
	def __init__(self, matrix_as_lists):
		if not isinstance(matrix_as_lists, list):
			raise ValueError("matrix_as_lists is not a list!")

		if len(matrix_as_lists) == 0:
			raise ValueError("matrix_as_lists is empty!")

		matrix_width = len(matrix_as_lists)
		for row_list in matrix_as_lists:
			if not isinstance(row_list, list):
				raise ValueError(f"{row_list} is not a list!")
			if len(row_list) != matrix_width:
				raise ValueError(f"{row_list} is not the right length")

		self.row_count = len(matrix_as_lists)
		self.col_count = len(matrix_as_lists[0])

		self._matrix_data = matrix_as_lists

	def get_raw_data(self):
		return(self._matrix_data)

	def print_info(self):
		print(f"Matrix has {self.row_count} rows")
		print(f"Matrix has {self.col_count} columns")

	def add(self, another_matrix):
		new = []
		for x in range(self.row_count):
			new.append([None]*self.col_count)
		print(new)
		for i in range(self.row_count):
			for j in range(self.col_count):
				new[i][j] = self.get_raw_data()[i][j] + another_matrix.get_raw_data()[i][j]
		return (Matrix(new))

	def __add__(self, other):
		return(self.add(other))


class Matrix_3_x_3(Matrix):
	def __init__(self, matrix_as_lists):
		super().__init__(matrix_as_lists)

		if len(matrix_as_lists)!=3:
			raise ValueError("3x3 matrix plz xox")
		for row_list in matrix_as_lists:
			if len(row_list) != 3:
				raise ValueError("3x3 matrix plz xox")
	def get_raw_data(self):
		return(self._matrix_data)

	def transpose(self):
		new = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
		for i in range(3):
			for j in range(3):
				new[i][j] = self.get_raw_data()[j][i]
		return(Matrix_3_x_3(new))

	def multiply(self, another_matrix):
		new = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
		for i in range(3):
			for j in range(3):
				new[i][j] = dot_product(self.get_raw_data()[i], another_matrix.transpose().get_raw_data()[j])
		return(Matrix_3_x_3(new))

	def __repr__(self):
		return(str(self.get_raw_data()))

	def __mul__(self, other):
		return(self.multiply(other))

	def det_two_by_two(self):
		return(self.get_raw_data()[1][1]*self.get_raw_data()[0][0] - self.get_raw_data()[0][1]*self.get_raw_data()[1][0])

	def determinant(self):
		det = 0
		for i in range(3):
			row1 = list(self.get_raw_data()[1])
			row2 = list(self.get_raw_data()[2])
			row1.pop(i)
			row2.pop(i)
			print(row1)
			print(row2)
			print(self.get_raw_data()[0][i])
			print(Matrix_3_x_3([row1, row2]).det_two_by_two())
			det = det + self.get_raw_data()[0][i]*(Matrix_3_x_3([row1, row2]).det_two_by_two())
		return(det)

#class det_1(Matrix_3_x_3):
#	super().__init__()


def dot_product(x,y):
	sum = 0
	for i in range(3):
		sum = sum + x[i] * y[i]
	return(sum)