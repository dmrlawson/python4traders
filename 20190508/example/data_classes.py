class Matrix:
	def __init__(self, matrix_as_lists):
		if not isinstance(matrix_as_lists, list):
			raise ValueError("matrix_as_lists is not a list!")

		if len(matrix_as_lists) == 0:
			raise ValueError("matrix_as_lists is empty!")

		matrix_width = len(matrix_as_lists)
		for row_list in matrix_as_lists:
			if not isinstance(row_list, list):
				raise ValueError(f"{row_list} is not a list!")
			if len(row_list) != matrix_width:
				raise ValueError(f"{row_list} is not the right length")

		self._matrix_data = matrix_as_lists
