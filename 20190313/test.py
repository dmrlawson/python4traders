import example

inputs_outputs = [
    ([4, 3, 2, 7, 8, 2, 3, 1], [2, 3]),
    ([4, 4, 2, 5, 6, 2, 7, 6, 8], [2, 4]),
    ([], []),
    ([1, 2, 3, 4, 5, 6, 7, 8, 9], [])
]

for test_input, output in inputs_outputs:
    actual_output = example.find_duplicates(test_input)
    if sorted(output) == sorted(actual_output):
        print("PASS")
    else:
        print(f"FAIL: wanted {sorted(output)}, got {sorted(actual_output)}")
