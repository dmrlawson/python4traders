def find_duplicates(a_list):

    temp_list = []
    dupe_list = set()

    for a in a_list:
        if a not in temp_list and a is not None:
            temp_list.append(a)
        else:
            dupe_list.add(a)

    return dupe_list


#
# DON'T TOUCH BELOW HERE
#
if __name__ == "__main__":
    inputs_outputs = [
        ([4, 3, 2, 7, 8, 2, 3, 1, 3], [2, 3]),
        ([4, 4, 2, 5, 6, 2, 7, 6, 8], [2, 4, 6]),
        ([], []),
        ([1, 2, 3, 4, 5, 6, 7, 8, 9], [])
    ]

    for test_input, output in inputs_outputs:
        actual_output = find_duplicates(test_input)
        if sorted(output) == sorted(actual_output):
            print("PASS")
        else:
            print(f"FAIL: wanted {sorted(output)}, got {sorted(actual_output)}")

