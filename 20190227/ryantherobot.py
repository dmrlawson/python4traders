import robot
import time
from robot import LEFT_FORWARD
from robot import RIGHT_FORWARD
from robot import RIGHT_BACKWARD
from robot import LEFT_BACKWARD
import requests



data = requests.get('https://dmrlawson.co.uk/robot_instructions.txt')
#data.text('utf8')
#for line in data.text.split('\n'):
instructions = data.text.split('\n')
with robot.RobotContext("192.168.1.148", 5432) as p:
    for i in instructions:
        direction = i.split(' ')[0]
        speed = float(i.split(' ')[1])
        if direction == 'FORWARD':
            p.go(LEFT_FORWARD | RIGHT_FORWARD, 64, 64)
            time.sleep(speed)
        elif direction == 'BACK':
            p.go(LEFT_BACKWARD | RIGHT_BACKWARD, 64, 64)
            time.sleep(speed)
        elif direction == 'RIGHT':
            p.go(LEFT_FORWARD | RIGHT_BACKWARD, 64, 64)
            time.sleep(speed)
        elif direction == 'LEFT':
            p.go(LEFT_BACKWARD | RIGHT_FORWARD, 64, 64)
            time.sleep(speed)

#with robot.RobotContext("192.168.1.148", 5432) as p:
#     p.go(LEFT_FORWARD | RIGHT_FORWARD, 128, 128)
#     time.sleep(1)
#     p.go(LEFT_FORWARD | RIGHT_BACKWARD, 255, 255)
#     time.sleep(1)
#     p.go(LEFT_FORWARD | RIGHT_FORWARD, 128, 255)
#     time.sleep(2)