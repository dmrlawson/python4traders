import socket
import struct
import time

# 192.168.1.108
# 192.168.1.97

LEFT_FORWARD = 0b10000000
LEFT_BACKWARD = 0b01000000
RIGHT_FORWARD = 0b00100000
RIGHT_BACKWARD = 0b00010000


class RobotContext:
	def __init__(self, host, port):
		self.host = host
		self.port = port
		self.sock = socket.socket()

	def __enter__(self):
		self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
		self.sock.connect((self.host, self.port))
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.go(0, 0, 0)
		self.sock.close()

	def go(self, direction, l_speed, r_speed):
		try:
			list_of_bytes = [
				struct.pack('>B', x) for x in [direction, l_speed, r_speed]
			]
			byte_string = b"".join(list_of_bytes)
			self.sock.send(byte_string)
		except Exception:
			self.sock.close()
			self.sock = socket.socket()
			self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
			self.sock.connect((self.host, self.port))


if __name__ == "__main__":
	with RobotContext("192.168.1.108", 5432) as r:
		r.go(LEFT_FORWARD | RIGHT_FORWARD, 128, 128)
		time.sleep(1)
		r.go(LEFT_FORWARD | RIGHT_BACKWARD, 255, 255)
		time.sleep(1)
		r.go(LEFT_FORWARD | RIGHT_FORWARD, 128, 255)
		time.sleep(2)
