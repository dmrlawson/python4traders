import robot
import time

import requests

url = 'https://dmrlawson.co.uk/robot_instructions.txt'
html = requests.get(url)
#
with robot.RobotContext("192.168.1.148", 5432) as r:

    # r.go(robot.LEFT_FORWARD | robot.RIGHT_FORWARD, 128, 128)
    # time.sleep(1)
#     r.go(robot.LEFT_FORWARD | robot.RIGHT_BACKWARD, 255, 255)
#     time.sleep(1)
#
#     r.go(robot.LEFT_FORWARD | robot.RIGHT_FORWARD, 128, 255)
#     time.sleep(2)

    for a in html.text.split('\n'):
        print(a)
        if a.split(' ')[0] == 'FORWARD':
            r.go (robot.LEFT_FORWARD | robot.RIGHT_FORWARD, 128, 128)
            time.sleep(int(a.split(' ')[1]))
        elif a.split(' ')[0] == 'RIGHT':
            r.go(robot.LEFT_FORWARD | robot.RIGHT_BACKWARD, 128, 128)
            time.sleep(int(a.split(' ')[1]))
        elif a.split(' ')[0] == 'LEFT':
            r.go(robot.RIGHT_FORWARD | robot.LEFT_BACKWARD, 128, 128)
            time.sleep(float(a.split(' ')[1]))
        elif a.split(' ')[0] == 'BACK':
            r.go(robot.LEFT_BACKWARD | robot.RIGHT_BACKWARD, 128, 128)
            time.sleep(int(a.split(' ')[1]))



# with robot.RobotContext("192.168.1.108", 5432) as r:
#     r.go(robot)
#
