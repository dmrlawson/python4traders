import robot
import time


import requests
url = 'https://dmrlawson.co.uk/robot_instructions.txt'
r = requests.get(url)
text = r.text
split_text = text.split()
directions = split_text[0::2]
speed = split_text[1::2]

print(directions[0])
print(int(speed[0])*50)


v = 180

with robot.RobotContext("192.168.1.108", 5432) as r:
	for i in range(len(directions)):
		if directions[i] == 'FORWARD':
			r.go(robot.LEFT_FORWARD | robot.RIGHT_FORWARD, v, v)
			time.sleep(float(speed[i]))
		if directions[i] == 'RIGHT':
			r.go(robot.LEFT_BACKWARD | robot.RIGHT_FORWARD, v, v)
			time.sleep(float(speed[i]))
		if directions[i] == 'LEFT':
			r.go(robot.LEFT_FORWARD | robot.RIGHT_BACKWARD, v, v)
			time.sleep(float(speed[i]))
		if directions[i] == 'BACK':
			r.go(robot.LEFT_BACKWARD | robot.RIGHT_BACKWARD, v, v)
			time.sleep(float(speed[i]))

