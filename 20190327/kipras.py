def number_to_text(a_number):
	single = ['one', 'two','three','four','five','six','seven','eight','nine']
	teen = ['eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen']
	decimal = ['ten','twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety']
	hundred = ['hundred']

	if a_number / 10 < 1:

			return single[a_number-1]

	elif a_number / 100 < 1:
		if 11 <= a_number <=19:
			return teen[a_number % 10 - 1]
		else:
			return decimal[int(a_number / 10) - 1], single[a_number % 10 - 1]

	elif a_number / 100 >= 1:
		return single[a_number ]

		'''
		if a_number == 2:
			return 'two'
		if a_number == 3:
			return 'three'
	elif len(a_number) == 2:

	elif len(a_number) == 3:

	return ""
'''


# DON'T TOUCH BELOW HERE
#
if __name__ == "__main__":
	inputs_outputs = [
		(1, "one"),
		(2, "two"),
		(3, "three"),
		(10, "ten"),
		(11, "eleven"),
		(13, "thirteen"),
		(24, "twenty four"),
		(35, "thirty five"),
		(46, "forty six"),
		(100, "one hundred"),
		(277, "two hundred and seventy seven"),
		(812, "eight hundred and twelve"),
	]

	for test_input, output in inputs_outputs:
		actual_output = number_to_text(test_input)
		if sorted(output) == sorted(actual_output):
			print("PASS")
		else:
			print(
				f"FAIL: wanted '{output}', got '{actual_output}'")
