def number_to_text(a_number):
	if a_number < 0:
		string = 'minus '
		a_number = 0 - a_number
		number_split = [int(d) for d in str(a_number)]
	else:
		number_split = [int(d) for d in str(a_number)]
		string = ''
	if len(number_split) == 1:
		number_split = [0,0] + number_split
	if len(number_split) == 2:
		number_split = [0] + number_split
	if number_split[0] == 0:
		string = string + ''
	if number_split[0] == 1:
		string = string + 'one hundred'
	if number_split[0] == 2:
		string = string + 'two hundred and '
	if number_split[0] == 3:
		string = string + 'three hundred and'
	if number_split[0] == 4:
		string = string + 'four hundred and'
	if number_split[0] == 5:
		string = string + 'five hundred and'
	if number_split[0] == 6:
		string = string + 'six hundred and'
	if number_split[0] == 7:
		string = string + 'seven hundred and '
	if number_split[0] == 8:
		string = string + 'eight hundred and '
	if number_split[0] == 9:
		string = string + 'nine hundred and'
	if number_split[1] == 0:
		string = string + ''
	if number_split[1] == 1 and number_split[2]==0:
		string = string + 'ten'
	if number_split[1] == 1 and number_split[2]==1:
		string = string + 'eleven'
	if number_split[1] == 1 and number_split[2]==2:
		string = string + 'twelve'
	if number_split[1] == 1 and number_split[2]==3:
		string = string + 'thirteen'
	if number_split[1] == 2:
		string = string + 'twenty '
	if number_split[1] == 3:
		string = string + 'thirty '
	if number_split[1] == 4:
		string = string + 'forty '
	if number_split[1] == 5:
		string = string + 'fifty '
	if number_split[1] == 6:
		string = string + 'sixty '
	if number_split[1] == 7:
		string = string + 'seventy '
	if number_split[1] == 8:
		string = string + 'eighty '
	if number_split[1] == 9:
		string = string + 'ninety '
	if number_split[2] == 0:
		string = string + ''
	if number_split[1] == 1 and number_split[2] in [0,1,2,3]:
		string = string + ''
	elif number_split[2] == 1:
		string = string + 'one'
	elif number_split[2] == 2:
		string = string + 'two'
	elif number_split[2] == 3:
		string = string + 'three'
	if number_split[2] == 4:
		string = string + 'four'
	elif number_split[2] == 5:
		string = string + 'five'
	elif number_split[2] == 6:
		string = string + 'six'
	elif number_split[2] == 7:
		string = string + 'seven'
	elif number_split[2] == 8:
		string = string + 'eight'
	elif number_split[1] == 9:
		string = string + 'nine'
	return string


#
# DON'T TOUCH BELOW HERE
#
if __name__ == "__main__":
	inputs_outputs = [
		(1, "one"),
		(2, "two"),
		(3, "three"),
		(10, "ten"),
		(11, "eleven"),
		(13, "thirteen"),
		(24, "twenty four"),
		(35, "thirty five"),
		(46, "forty six"),
		(100, "one hundred"),
		(277, "two hundred and seventy seven"),
		(812, "eight hundred and twelve"),
		(-3, "minus three"),
	]

	for test_input, output in inputs_outputs:
		actual_output = number_to_text(test_input)
		if sorted(output) == sorted(actual_output):
			print("PASS")
		else:
			print(
				f"FAIL: wanted '{output}', got '{actual_output}'")
