def num_digits(number):
	if number / 10.0 < 1:
		return 1
	else:
		number = number/10.0
		return num_digits(number) +1


def number_to_text(a_number):
	alist = []
	no_1 = "one"
	no_2 = "two"
	no_3 = "three"
	no_4 = "four"
	no_5 = "five"
	no_6 = "six"
	no_7 = "seven"
	no_8 = "eight"
	no_9 = "nine"
	if num_digits(a_number) == 1:
		if a_number == 1:
			return no_1
		elif a_number == 2:
			return no_2
		elif a_number == 3:
			return no_3
		elif a_number == 4:
			return no_4
		elif a_number == 5:
			return no_5
		elif a_number == 6:
			return no_6
		elif a_number == 7:
			return no_7
		elif a_number == 8:
			return no_8
		elif a_number == 9:
			return no_9
	elif num_digits(a_number) == 2:
		str_a_number = str(a_number)
		for i in str_a_number:
			alist.append(i)
		if alist[0] == 2:
			teens = "teen"
			if alist[1] =


#
# DON'T TOUCH BELOW HERE
#
if __name__ == "__main__":
	inputs_outputs = [
		(1, "one"),
		(2, "two"),
		(3, "three"),
		(10, "ten"),
		(11, "eleven"),
		(13, "thirteen"),
		(24, "twenty four"),
		(35, "thirty five"),
		(46, "forty six"),
		(100, "one hundred"),
		(277, "two hundred and seventy seven"),
		(812, "eight hundred and twelve"),
	]

	for test_input, output in inputs_outputs:
		actual_output = number_to_text(test_input)
		if (output) == (actual_output):
			print("PASS")
		else:
			print(
				f"FAIL: wanted '{output}', got '{actual_output}'")
