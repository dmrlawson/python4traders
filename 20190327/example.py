def number_to_text(a_number):
	return ""


#
# DON'T TOUCH BELOW HERE
#
if __name__ == "__main__":
	inputs_outputs = [
		(1, "one"),
		(2, "two"),
		(3, "three"),
		(10, "ten"),
		(11, "eleven"),
		(13, "thirteen"),
		(24, "twenty four"),
		(35, "thirty five"),
		(46, "forty six"),
		(100, "one hundred"),
		(277, "two hundred and seventy seven"),
		(812, "eight hundred and twelve"),
	]

	for test_input, output in inputs_outputs:
		actual_output = number_to_text(test_input)
		if sorted(output) == sorted(actual_output):
			print("PASS")
		else:
			print(
				f"FAIL: wanted '{output}', got '{actual_output}'")
