import time


def make_list(x, num_leds):
	led_list = [(255,255,255)] * num_leds
	if x < 50:
		led_list[x:x+10] = [(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255),(0,0,255)]
	else:
		led_list[x:x+10] = [(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0),(0,255,0)]
	
	return led_list

def run(client, num_leds):

	while True:
		for i in range(num_leds):
				led_list = make_list(i,num_leds)
				led_list = led_list[0:num_leds]
		

		  #  print(len(led_list))

				client.send_all(led_list)

	#    for each in range(led_list):
