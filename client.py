import logging
import os
import socket
import struct
import sys

LOGGER = logging.getLogger(__name__)

BLACK = (0, 0, 0)
WHITE = (100, 100, 100)
RED = (100, 0, 0)
GREEN = (0, 100, 0)
BLUE = (0, 0, 100)


class Client(object):
    SET_COMMAND = 0x00
    SHOW_COMMAND = 0xff

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket()
        self.connect()

    def __enter__(self):
        return self

    def connect(self):
        self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
        self.sock.connect((self.host, self.port))
        LOGGER.info('connected to %s:%d', self.host, self.port)

    def int_list_to_byte_string(self, int_list):
        byte_string = b''
        for i in int_list:
            byte_string += struct.pack('>B', i)
        return byte_string

    def send_all(self, list_of_leds):
        byte_list = []
        byte_list.append(self.SET_COMMAND)
        length_low = (len(list_of_leds) >> 8) & 0xff
        length_high = len(list_of_leds) & 0xff
        byte_list.append(length_low)
        byte_list.append(length_high)

        for red, green, blue in list_of_leds:
            red_8 = red & 0xff
            green_8 = green & 0xff
            blue_8 = blue & 0xff
            byte_list.append(red_8)
            byte_list.append(green_8)
            byte_list.append(blue_8)
        byte_list.append(self.SHOW_COMMAND)

        # sendall raises if not all data was sent
        try:
            self.sock.sendall(self.int_list_to_byte_string(byte_list))
        except Exception as e:
            LOGGER.exception(e)
            self.connect()

    def __exit__(self, exc_type, exc_value, traceback):
        self.sock.close()
        LOGGER.info('disconnected')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    if len(sys.argv) < 2:
        print('python3 -m client [module]')
        sys.exit(0)

    HOST = os.environ.get('HOST', 'derp')
    PORT = int(os.environ.get('PORT', 2812))
    NUM_LEDS = int(os.environ.get('NUM_LEDS', 150))

    try:
        with Client(HOST, PORT) as led:
            __import__(sys.argv[1], fromlist=['']).run(led, NUM_LEDS)
    except KeyboardInterrupt:
        LOGGER.info('quitting...')
