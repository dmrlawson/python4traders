import time
import random


def set_led(leds, index, colour):
    leds[index] = colour


def run(client, num_leds):
    leds = [(0, 0, 0)] * num_leds
    blue = (0, 0, 255)
    green = (0, 255, 0)
    red = (255, 0, 0)
    list1 = []

    for i in range(num_leds):

        set_led(leds, random_position(list1,num_leds), random_red_colour())

        time.sleep(0.05)
        client.send_all(leds)

def random_colour():
    return (int(random.random() * 255), int(random.random() * 255), int(random.random() * 255))

def random_red_colour():
    random_red = random_colour()
    while random_red[0] < 200:
        random_red = random_colour()
    return random_red

def random_position(list,num_leds):
    random_led_number = int(random.random() * num_leds)
    while random_led_number in list:
        random_led_number = int(random.random() * num_leds)
    list.append(random_led_number)
    return random_led_number