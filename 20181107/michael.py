import time
import random

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)


def set_list(leds, index, colour):
    if 0 <= index < len(leds):
        leds[index] = colour
    else:
        print('invalid index')


def set_list2(leds, index, colour):
    try:
        leds[index] = colour
    except IndexError:
        print('invalid index')


def random_colour():
    rcol = (int(random.random() * 255), int(random.random() * 255), int(random.random() * 255))
    while rcol[1]>10 and rcol[2]>10:
        rcol = (int(random.random() * 255), int(random.random() * 255), int(random.random() * 255))
    return rcol

def random_led(num_leds):
    return int(random.random() * num_leds)


def run(client, num_leds):
    led_list = [(255, 255, 255)] * num_leds
    list = []
    for i in range(num_leds):
        col = random_colour()
        led = random_led(num_leds)
        while led in list:
            led = random_led(num_leds)
        while col[0]<230:
            col = random_colour()
        set_list2(led_list, led, col)
        list.append(led)
        client.send_all(led_list)
        time.sleep(0)
    print(len(list))
#    for each in range(led_list)