import time

def run(client, num_leds):

   while True:
        led_list = []
        for each in range(num_leds):
            if each % 3 == 0:
                led_list.append((255, 255, 0))

            elif each % 3 == 1:
                led_list.append((0, 100, 0))

            else:
                led_list.append((100, 0, 0))

      #  print(len(led_list))

        client.send_all(led_list)

#    for each in range(led_list):
