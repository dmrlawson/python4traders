import time

def run(client, num_leds):

    num_leds = 150
    R0 = 255
    G0 = 0
    led_list = []
    t = 1
    
    while t<1234:   # change colour over time (random length of 12.34 seconds)
    
        #print "-------"    #debug
        #print "t%s" % (t)    #debug
        
        dRt = t *5
        dGt = t *5
        r = R0 + dRt    #lower case variable for if condition checking only
        g = G0 + dGt    #lower case variable for if condition checking only
        
        if r%255 == 0:
            R = ((r//255))%2*255
        elif (r//255)%2 == 1:
            #print "R1"    #debug
            R = -(r%255 -255)
        else:
            #print "R0"    #debug
            R = r%255
        
        if g%255 == 0:
            G = ((g//255)%2)*255
        elif (g//255)%2 == 1:
            #print "G1"    #debug
            G = -(g%255 -255)
        else:
            #print "G0"    #debug
            G = g%255
        
        led0 = [R, G, 0]
        #print "led0 = ", led0    #debug
        
    
        for x in range(num_leds): #gradient over physical length of strip
            
            dRx = x *5
            dGx = x *5
            rr = R + dRx    #lower case variable for if condition checking only
            gg = G + dGx    #lower case variable for if condition checking only
            
            if rr%255 == 0:
                Rx = ((rr//255))%2*255
            elif (rr//255)%2 == 1:
                Rx = -(rr%255 -255)
            else:
                Rx = rr%255
        
            if gg%255 == 0:
                Gx = ((gg//255)%2)*255
            elif (gg//255)%2 == 1:
                Gx = -(gg%255 -255)
            else:
                Gx = gg%255
            
            ledx = [Rx,Gx,0]
            led_list.append(ledx)
            #print "x = ",x     #debug
            #print ledx     #debug
        
        #print     #debug
        #print led_list     #debug
        
        client.send_all(led_list)
        led_list = []
        time.sleep(0.01)
        t = t + 1
    
    
    led = [255,255,255]     # flash white 3 times after loop finishes seconds
    led_list2 = []
    for z in range(num_leds):
        led_list2.append(led)
    time.sleep(1)
    client.send_all(led_list2)
    time.sleep(0.5)
    client.send_all(led_list2)
    time.sleep(0.05)
    client.send_all(led_list2)
