import time

def run(client, num_leds):

  led_list = []
  a = [260,0,0]

  t = 0

  while t <= 50:
    for i in range(51):
         for ii in range(num_leds):
            led_list.append(a[0]-5, a[1]+5, 0)
            client.send_all(led_list)
         time.sleep(0.1)
         a = led_list[0]
    t = t + 1

    t = t - 1    # when t = 51

  while t >= 1
    for i in range(51):
         for ii in range(num_leds):
            led_list.append(a[0]+5, a[1]-5, 0)
            client.send_all(led_list)
         time.sleep(0.1)
         a = led_list[0]
    t = t - 1
