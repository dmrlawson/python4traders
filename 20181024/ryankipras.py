import time
import random


def set_led(leds, index, colour):
    leds[index] = colour


def run(client, num_leds):
    leds = [(0, 0, 0)] * num_leds
    blue = (0, 0, 255)
    green = (0, 255, 0)
    red = (255, 0, 0)
    list1 = []

    for i in range(num_leds):
        random_led_number = int(random.random() * num_leds)

        while random_led_number in list1:
            random_led_number = int(random.random() * num_leds)
        list1.append(random_led_number)
        set_led(leds, random_led_number, random_colour())

        time.sleep(0.05)
        client.send_all(leds)

def random_colour():
    return (int(random.random() * 255), int(random.random() * 255), int(random.random() * 255))