import timeit

setup1 = """
def set_list(leds, index, colour):
    if 0 <= index < len(leds):
        leds[index] = colour
    else:
        pass
"""

setup2 = """
def set_list2(leds, index, colour):
    try:
        leds[index] = colour
    except IndexError:
        pass
"""



print(timeit.timeit('set_list([(255, 255, 255)] * 250, 10, (0,0,0))', number=10000000, setup=setup1))
print(timeit.timeit('set_list([(255, 255, 255)] * 250, 400, (0,0,0))', number=10000000, setup=setup1))
print(timeit.timeit('set_list2([(255, 255, 255)] * 250, 10, (0,0,0))', number=10000000, setup=setup2))
print(timeit.timeit('set_list2([(255, 255, 255)] * 250, 400, (0,0,0))', number=10000000, setup=setup2))