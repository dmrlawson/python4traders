import time
import random


BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)


def set_list(leds, index, colour):
    if 0 <= index < len(leds):
        leds[index] = colour
    else:
        print('invalid index')

def set_list2(leds, index, colour):
    try:
        leds[index] = colour
    except IndexError:
        print('invalid index')



def random_colour():
    return (int(random.random() * 255), int(random.random() * 255), int(random.random() * 255))

def run(client, num_leds):
    led_list = [(255, 255, 255)] * num_leds
    for i in range(250):
        set_list2(led_list, int(random.random() * num_leds)+100, random_colour())

        #  print(len(led_list))
        client.send_all(led_list)
        time.sleep(0)

#    for each in range(led_list):



