import random
import time
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)

def random_colour():
    rcol = (int(random.random()*255), int(random.random()), int(random.random()))
    return rcol


def run(client, num_leds):
    d=[random.randint(0,1) for i in range(num_leds)]
    print(d)
    led_list = [(255*i,255*i,255*i) for i in d]
    print(led_list)
    client.send_all(led_list)